---
title: File Formats
weight: 17
pre: "<i class='fas fa-chevron-right'></i> "
---


== Legacy Format Documentation
New file format documentation is currently a work in progress.

Older documentation for the non-sexpr schematic file format and older pcb sexpr format is link:legacy_file_format_documentation.pdf[available here]